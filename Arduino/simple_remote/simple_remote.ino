#include <ESP8266WiFi.h> // Library for using the NodeMCU 
#include <PubSubClient.h> // Library for using the Network protcall MQTT in this case
#include <RCSwitch.h>
#include "pass.h"

/************************* WiFi Access Point *********************************/
const char* ssid = getwifissid(); // Add your SSID(The Name of the Wifi Router you connect to)
const char* password = getwifipass(); // The PassWord (Wireless Key)

/************************* LinuxMqtt Setup *********************************/
const char* mqtt_server = "broker.hivemq.com";
WiFiClient espClient;
PubSubClient client(espClient);

/************************* RCSwitch Setup *********************************/


void happyblink(int amount){
  for(int i = 0;i < amount; i++){
    digitalWrite(LED_BUILTIN,!digitalRead(LED_BUILTIN));
    delay(100);
  }
  digitalWrite(LED_BUILTIN,HIGH);
}

void setup_wifi() {
  pinMode(LED_BUILTIN, OUTPUT);
  delay(100);
  // We start by connecting to a WiFi network
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  happyblink(10);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    Serial.print("ClientId:");
    Serial.println(clientId);
    // Attempt to connect
    //if you MQTT broker has clientID,username and password
    //please change following line to    if (client.connect(clientId,userName,passWord))
    if (client.connect(clientId.c_str()))
    {
      Serial.println("connected");
      happyblink(2);
      Serial.println(client.subscribe("esp826601/RCSwitch"));

    }
    else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 6 seconds before retrying
      delay(6000);
    }
  }
} //end reconnect()


void setup() {
  Serial.begin(9600); // Starts the serial monitor at 115200
  setup_wifi(); // add Method for the Wifi setup
  client.setServer(mqtt_server, 1883); // connects to the MQTT broker
  client.setCallback(callback);
  

}

void callback(char* topic, byte* message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  happyblink(1);
  Serial.print(topic);
  Serial.print(". Message: ");
  String messageRemote;
  for (int i = 0; i < length; i++) {
    Serial.println((char)message[i]);
    messageRemote += (char)message[i];
  }

  if(String(topic) == "esp826601/RemoteID") {
    Serial.println("Remote Control");
  }
}
void loop() {

    if (!client.connected()) {
    reconnect();
  } // MQTT
  client.loop();

}
